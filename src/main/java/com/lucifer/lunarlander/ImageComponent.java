/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucifer.lunarlander;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import static java.lang.reflect.Array.set;
import javax.swing.JComponent;

/**
 *
 * @author lucifer
 */
public class ImageComponent extends JComponent {

    private BufferedImage image;

    public ImageComponent(int w, int h) {
        image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(image, 0, 0, null);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(image.getWidth(), image.getHeight());
    }

    @Override
    public Dimension getSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public BufferedImage getImage() {
        return image;
    }

}
