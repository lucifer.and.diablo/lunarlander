/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucifer.lunarlander;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author lucifer
 */
public class LunarLanderSimulator {

    public static final double GRAVITY = 1.62;
    public static final double THRUST = 10.0;
    public static final double TERMINAL_VELOCITY = 40;

    private double fuel;
    private double time;
    private double altitude;
    private double attitude;
    private double velocityX;
    private double velocityY;
    private double axis;
    private double thrust;
    private boolean damaged;

    private int corrections = 0;

    private final static AtomicInteger ai = new AtomicInteger(0);

    public LunarLanderSimulator() {
        this.fuel = 200;
        this.time = 0;
        this.altitude = 10000;
//        this.attitude = ai.incrementAndGet() % 2 == 0 ? -5000 : 5000;//Math.random() * 10000 - 5000;
//        this.attitude = Math.random() * 10000 - 5000;
        this.attitude = -5000;
        this.velocityX = 0;
        this.velocityY = 0;
        this.axis = 0;
        this.damaged = false;
        this.thrust = 0;
    }

    public void cycle() {
        this.time++;
        this.velocityY -= GRAVITY;

        double axisX = Math.sin(axis * Math.PI / 180.0);
        double axisY = Math.cos(axis * Math.PI / 180.0);

        if (thrust < 0) {
            thrust = 0;
        }

        if (thrust > 0 && fuel > 0) {

            fuel -= thrust / 10;
            this.velocityX += axisX * thrust;
            this.velocityY += axisY * thrust;
        } else {
            fuel = 0;
            thrust = 0;
        }

        if (Math.abs(this.velocityX) > TERMINAL_VELOCITY / 2.0) {
            this.velocityX += velocityX > 0 ? -GRAVITY / 100.0 : GRAVITY / 100.0;
        }

        this.velocityX = Math.min(TERMINAL_VELOCITY, Math.max(-TERMINAL_VELOCITY, this.velocityX));
        this.velocityY = Math.min(TERMINAL_VELOCITY, Math.max(-TERMINAL_VELOCITY, this.velocityY));

        this.altitude += this.velocityY;
        this.attitude += this.velocityX;

        if (altitude < 0) {
            altitude = 0;
            if (velocityY < -5) {
                damaged = true;
            }
            if (axis > 94 || axis < 86) {
                damaged = true;
            }
        }
    }

    public double score() {
        // TODO: Создайте правильную функцию
        return 0;
    }

    public boolean isFlying() {
        return !damaged || altitude > 0;
    }

    public void setThrust(double thrust) {
        this.thrust = (thrust * THRUST);
    }

    public void setAxis(double axis) {

        this.axis += (axis - 0.5) / 1.0;
        corrections += Math.abs(axis - 0.5) * 10;
    }

    public double getDistance() {
        return Math.sqrt(attitude * attitude + altitude * altitude) + 1;
    }

    public boolean isDamaged() {
        return damaged;
    }

    public double getAltitude() {
        return altitude;
    }

    public double getAttitude() {
        return attitude;
    }

    public double getAxis() {
        return axis % 360;
    }

    public double getFuel() {
        return fuel;
    }

    public double getThrust() {
        return thrust;
    }

    public double getTime() {
        return time;
    }

    public double getVelocityY() {
        return velocityY;
    }

    public double getVelocityX() {
        return velocityX;
    }

}
