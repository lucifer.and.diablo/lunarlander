/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucifer.lunarlander;

import org.encog.ml.CalculateScore;
import org.encog.ml.MLMethod;

/**
 *
 * @author lucifer
 */
public class LunarLanderScore implements CalculateScore {

    private final boolean debug;

    public LunarLanderScore() {
        debug = false;
    }

    public LunarLanderScore(boolean debug) {
        this.debug = debug;
    }

    @Override
    public double calculateScore(MLMethod method) {
        LunarLanderPilot pilot = new LunarLanderPilot(method, debug);
        return pilot.score();
    }

    @Override
    public boolean shouldMinimize() {
        return false;
    }

    @Override
    public boolean requireSingleThreaded() {
        return false;
    }

}
