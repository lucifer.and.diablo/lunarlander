/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucifer.lunarlander;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import javax.swing.SwingUtilities;
import org.encog.ml.MLMethod;
import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.neat.NEATNetwork;
import org.encog.neural.networks.BasicNetwork;
import org.encog.util.arrayutil.NormalizationAction;
import org.encog.util.arrayutil.NormalizedField;

/**
 *
 * @author lucifer
 */
public class LunarLanderPilot {

    private final MLMethod network;
    private final NormalizedField fuel;
    private final NormalizedField altitude;
    private final NormalizedField attitude;
    private final NormalizedField velocityX;
    private final NormalizedField velocityY;
    private final NormalizedField axis;
    private final NormalizedField thrust;
    private final boolean debug;

    LunarLanderPilot(MLMethod method, boolean debug) {
        this.network = method;
        this.debug = debug;

        this.fuel = new NormalizedField(NormalizationAction.Normalize, "fuel", 200, 0, -0.9, 0.9);
        this.altitude = new NormalizedField(NormalizationAction.OneOf, "altitude", 10000, 0, -0.9, 0.9);
        this.attitude = new NormalizedField(NormalizationAction.OneOf, "attitude", 10000, -10000, -0.9, 0.9);
        this.velocityX = new NormalizedField(NormalizationAction.OneOf, "velocityX", LunarLanderSimulator.TERMINAL_VELOCITY, -LunarLanderSimulator.TERMINAL_VELOCITY, -0.9, 0.9);
        this.velocityY = new NormalizedField(NormalizationAction.OneOf, "velocityY", LunarLanderSimulator.TERMINAL_VELOCITY, -LunarLanderSimulator.TERMINAL_VELOCITY, -0.9, 0.9);
        this.axis = new NormalizedField(NormalizationAction.OneOf, "axis", 360, 0, -0.9, 0.9);
        this.thrust = new NormalizedField(NormalizationAction.OneOf, "thrust", LunarLanderSimulator.THRUST, 0, -0.9, 0.9);
    }

    public double score() {
        LunarLanderSimulator lls = new LunarLanderSimulator();
        MLData input = new BasicMLData(7);
        while (lls.isFlying()) {
            input.setData(0, this.fuel.normalize(lls.getFuel()));
            input.setData(1, this.altitude.normalize(lls.getAltitude()));
            input.setData(2, this.attitude.normalize(lls.getAttitude()));
            input.setData(3, this.velocityX.normalize(lls.getVelocityX()));
            input.setData(4, this.velocityY.normalize(lls.getVelocityY()));
            input.setData(5, this.axis.normalize(lls.getAxis()));
            input.setData(6, this.thrust.normalize(lls.getThrust()));

            MLData out;
            if (network instanceof BasicNetwork) {
                out = ((BasicNetwork) this.network).compute(input);
            } else {
                out = ((NEATNetwork) this.network).compute(input);
            }
            lls.setThrust(out.getData(1));
            lls.setAxis(out.getData(0));
            lls.cycle();

            if (debug) {
                drawSim(lls);
            }
        }

        return lls.score();
    }

    private Font font = Font.decode("Tahoma-PLAIN-12");

    private void drawSim(LunarLanderSimulator lls) {
        BufferedImage img = Application.image.getImage();
        Graphics2D gr = (Graphics2D) img.getGraphics();

        gr.setColor(Color.BLACK);
        gr.fillRect(0, 0, img.getWidth(), img.getHeight());
        gr.setColor(Color.WHITE);

        AffineTransform at = gr.getTransform();

//        at.scale(0, 0);
        gr.translate(img.getWidth() / 2.0 + lls.getAttitude() / 20000.0 * 480.0, img.getHeight() - lls.getAltitude() / 20000.0 * 480.0 - 5);
        double max = 1.0 / (lls.getDistance() / 10000.0 * 480.0);
        if (max > 1) {
            max = 1;
        }
//        gr.scale(max, max);
        gr.rotate(lls.getAxis() * Math.PI / 180.0);
//        gr.scale(4, 4);
        gr.drawRect(-5, -5, 10, 10);
        gr.drawLine(0, 5, 0, 5 + (int) (lls.getThrust() * 4));

//        gr.drawLine(0, 0, (int) -lls.getAttitude(), (int) lls.getAltitude());
        gr.setTransform(at);

        gr.setFont(font);

        int y = 16;
        gr.drawString("Altitude: " + lls.getAltitude(), 10, y += 16);
        gr.drawString("Attitude: " + lls.getAttitude(), 10, y += 16);
        gr.drawString("Thrust:   " + lls.getThrust(), 10, y += 16);
        gr.drawString("Angle:    " + lls.getAxis(), 10, y += 16);
        gr.drawString("Fuel:     " + lls.getFuel(), 10, y += 16);
        gr.drawString("Distance: " + lls.getDistance(), 10, y += 16);
        gr.drawString("VX:       " + lls.getVelocityX(), 10, y += 16);
        gr.drawString("VY:       " + lls.getVelocityY(), 10, y += 16);

//        System.out.println(lls.getAttitude() + " " + lls.getAltitude());
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Application.image.repaint();
            }
        });

        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(1000 / 60));
    }

}
