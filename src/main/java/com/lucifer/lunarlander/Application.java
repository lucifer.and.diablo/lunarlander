/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucifer.lunarlander;

import java.io.File;
import javax.swing.JFrame;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.ml.ea.train.EvolutionaryAlgorithm;
import org.encog.neural.hyperneat.HyperNEATCODEC;
import org.encog.neural.neat.NEATNetwork;
import org.encog.neural.neat.NEATPopulation;
import org.encog.neural.neat.NEATUtil;
import org.encog.neural.neat.training.species.OriginalNEATSpeciation;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.pattern.FeedForwardPattern;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.persist.EncogPersistor;
import org.encog.util.simple.EncogUtility;

/**
 *
 * @author lucifer
 */
public class Application {

    public static final ImageComponent image = new ImageComponent(640, 480);

    public static BasicNetwork createNetwork() {
        FeedForwardPattern pattern = new FeedForwardPattern();
        pattern.setInputNeurons(7);
//        pattern.addHiddenLayer(50);
        pattern.addHiddenLayer(250);
        pattern.setOutputNeurons(2);
        pattern.setActivationFunction(new ActivationTANH());
        BasicNetwork network = (BasicNetwork) pattern.generate();
        network.reset();
        return network;
    }

    public static void main(String[] args) {
        createFrame();
        BasicNetwork network = createNetwork();

//        MLTrain train = new MLMethodGeneticAlgorithm(new MethodFactory() {
//            @Override
//            public MLMethod factor() {
//                final BasicNetwork result = createNetwork();
//                ((MLResettable) result).reset();
//                return result;
//            }
//        }, new LunarLanderScore(), 500);;
//        MLTrain train = new NeuralSimulatedAnnealing(network, new LunarLanderScore(), 10, 2, 100);
//        Substrate substrate = SubstrateFactory.factorSandwichSubstrate(7, 2);
        LunarLanderScore score = new LunarLanderScore();
        NEATPopulation pop = new NEATPopulation(7, 2, 100);
        pop.setActivationCycles(40);
        pop.setInitialConnectionDensity(0.9);
        pop.setWeightRange(0.9);
        pop.setSurvivalRate(0.7);
        pop.reset();
        EvolutionaryAlgorithm train = NEATUtil.constructNEATTrainer(pop, score);
        OriginalNEATSpeciation speciation = new OriginalNEATSpeciation();
        speciation.setCompatibilityThreshold(0.3);
        train.setSpeciation(speciation);

        int epoch = 1;

        for (int i = 0; i < 5000; i++) {
            for (int t = 0; t < 10; t++) {
                train.iteration();
            }
            System.out.println("Epoch #" + epoch + " Score:" + train.getError());
            if (i % 100 == 99) {
                NEATNetwork phenotype = (NEATNetwork) train.getCODEC().decode(train.getBestGenome());
                EncogDirectoryPersistence.saveObject(new File(Math.round(train.getError()) + ".net"), train.getPopulation());
                new LunarLanderPilot(phenotype, true).score();
            }
            epoch++;
        }
        train.finishTraining();

        Encog.getInstance().shutdown();
    }

    private static void createFrame() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(image);
        frame.pack();
        frame.setVisible(true);
    }
}
