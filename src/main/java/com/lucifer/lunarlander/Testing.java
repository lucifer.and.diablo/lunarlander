/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucifer.lunarlander;

import static com.lucifer.lunarlander.Application.image;
import java.io.File;
import javax.swing.JFrame;
import org.encog.neural.neat.NEATNetwork;
import org.encog.neural.neat.NEATPopulation;
import org.encog.persist.EncogDirectoryPersistence;

/**
 *
 * @author lucifer
 */
public class Testing {

    public static void main(String[] args) {
        NEATPopulation o = (NEATPopulation) EncogDirectoryPersistence.loadObject(new File("c973.net"));
        NEATNetwork net = (NEATNetwork) o.getCODEC().decode(o.getBestGenome());
        createFrame();
        new LunarLanderPilot(net, true).score();
    }

    private static void createFrame() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(image);
        frame.pack();
        frame.setVisible(true);
    }
}
